import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Axios from 'axios';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'column',
    margin: 'auto',
    width: '50vw'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
});

class FilledTextFields extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      REGISTRATION_NUMBER: '',
      MODEL_NAME: '',
      MAKE: '',
      MODEL_YEAR: 0,
      MILEAGE: 0,
      CAR_CATEGORY_NAME: '',
      LOC_ID: '',
      AVAILABILITY_FLAG: 'A'
    };
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  handleSubmit = () => {
    Axios.post('http://localhost:5000/add/vehicle', this.state).then((res) => {
      console.log(res)
    }).catch((err) => {
      console.log(err)
    })
    this.setState({
      REGISTRATION_NUMBER: '',
      MODEL_NAME: '',
      MAKE: '',
      MODEL_YEAR: 0,
      MILEAGE: 0,
      CAR_CATEGORY_NAME: '',
      LOC_ID: '',
      AVAILABILITY_FLAG: 'A'
    })
  }

  render() {
    const { classes } = this.props;

    if(this.props.visible !== 1) {
        return null;
    }

    return (
      <form className={classes.container} noValidate autoComplete="off">
        <TextField
          id="filled-name"
          label="REGISTRATION_NUMBER"
          className={classes.textField}
          value={this.state.REGISTRATION_NUMBER}
          onChange={this.handleChange('REGISTRATION_NUMBER')}
          margin="normal"
          variant="filled"
        />
        <TextField
          id="filled-name"
          label="MODEL_NAME"
          className={classes.textField}
          value={this.state.MODEL_NAME}
          onChange={this.handleChange('MODEL_NAME')}
          margin="normal"
          variant="filled"
        />
        <TextField
          id="filled-name"
          label="MAKE"
          className={classes.textField}
          value={this.state.MAKE}
          onChange={this.handleChange('MAKE')}
          margin="normal"
          variant="filled"
        />
        <TextField
          id="filled-name"
          label="MODEL_YEAR"
          type="number"
          className={classes.textField}
          value={this.state.MODEL_YEAR}
          onChange={this.handleChange('MODEL_YEAR')}
          margin="normal"
          variant="filled"
        />
        <TextField
          id="filled-name"
          label="MILEAGE"
          type="number"
          className={classes.textField}
          value={this.state.MILEAGE}
          onChange={this.handleChange('MILEAGE')}
          margin="normal"
          variant="filled"
        />
        <TextField
          id="filled-name"
          label="CAR_CATEGORY_NAME"
          className={classes.textField}
          value={this.state.CAR_CATEGORY_NAME}
          onChange={this.handleChange('CAR_CATEGORY_NAME')}
          margin="normal"
          variant="filled"
        />
        <TextField
          id="filled-name"
          label="LOC_ID"
          className={classes.textField}
          value={this.state.LOC_ID}
          onChange={this.handleChange('LOC_ID')}
          margin="normal"
          variant="filled"
        />
        <Button onClick={this.handleSubmit} variant="contained" color="primary" className={classes.button}>
          Add Vehicle
        </Button>
      </form>
    );
  }
}

FilledTextFields.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FilledTextFields);
