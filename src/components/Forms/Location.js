import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
});

const Customer = (props) => {

    const { classes } = props;

    if(props.visible !== 1) {
        return null;
    }

    return (
      <form noValidate autoComplete="off">
        <TextField
          id="filled-name"
          label="Pick up location"
          className={classes.textField}
          value={props.pickupValue}
          onChange={props.pickup}
          margin="normal"
          variant="filled"
        />
        <TextField
          id="filled-name"
          label="Drop off location"
          className={classes.textField}
          value={props.dropValue}
          onChange={props.drop}
          margin="normal"
          variant="filled"
        />
      </form>
    );
}

Customer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Customer);
