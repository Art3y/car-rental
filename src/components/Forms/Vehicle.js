import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
});

const Customer = (props) => {

    const { classes } = props;

    if(props.visible !== 2) {
        return null;
    }

    return (
      <form noValidate autoComplete="off">
        <TextField
          id="filled-name"
          label="Vehicle"
          className={classes.textField}
          value={props.vehicleValue}
          onChange={props.vehicle}
          margin="normal"
          variant="filled"
        />
      </form>
    );
}

Customer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Customer);
