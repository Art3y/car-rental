import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { AppBar } from '@material-ui/core';

import Booking from './Booking'
import AddCustomer from './AddCustomer';
import AddVehicle from './AddVehicle';

const styles = {
    root: {
        flexGrow: 1,
        // margin: 'auto'
    },
};

class TabHeader extends React.Component {
    state = {
        value: 0,
    };

    handleChange = (event, value) => {
        this.setState({ value });
    };

    render() {
        const { classes } = this.props;

        return (
        <Paper className={classes.root}>
            <AppBar position="static" >
                <Tabs
                value={this.state.value}
                onChange={this.handleChange}
                centered >
                    <Tab label="New Customer" />
                    <Tab label="New Vehicle" />
                    <Tab label="Booking Vehicle" />
                </Tabs>
            </AppBar>
            <Booking visible={this.state.value} />
            <AddCustomer visible={this.state.value} />
            <AddVehicle visible={this.state.value} />
        </Paper>
        );
    }
}

TabHeader.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TabHeader);
