import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Axios from 'axios';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'column',
    margin: 'auto',
    width: '50vw'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
});

class FilledTextFields extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      FNAME: '',
      MNAME: '',
      LNAME: '',
      PHONE_NUMBER: 0,
      EMAIL_ID: '',
      STREET: '',
      CITY: '',
      STATE_NAME: '',
      ZIPCODE: '',
      MEMBERSHIP_TYPE: '',
      MEMBERSHIP_ID: '',
      DL_NUMBER: '',
    };
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  handleSubmit = () => {
    Axios.post('http://localhost:5000/add/customer', this.state).then((res) => {
      console.log(res)
    }).catch((err) => {
      console.log(err)
    })
    this.setState({
      FNAME: '',
      MNAME: '',
      LNAME: '',
      PHONE_NUMBER: 0,
      EMAIL_ID: '',
      STREET: '',
      CITY: '',
      STATE_NAME: '',
      ZIPCODE: '',
      MEMBERSHIP_TYPE: '',
      MEMBERSHIP_ID: '',
      DL_NUMBER: '',
    })
  }

  render() {
    const { classes } = this.props;

    if(this.props.visible !== 0) {
        return null;
    }

    return (
      <form className={classes.container} noValidate autoComplete="off">
        <TextField
          id="filled-name"
          label="DL NUMBER"
          className={classes.textField}
          value={this.state.DL_NUMBER}
          onChange={this.handleChange('DL_NUMBER')}
          margin="normal"
          variant="filled"
        />
        <TextField
          id="filled-name"
          label="FIRST NAME"
          className={classes.textField}
          value={this.state.FNAME}
          onChange={this.handleChange('FNAME')}
          margin="normal"
          variant="filled"
        />
        <TextField
          id="filled-name"
          label="MIDDLE NAME"
          className={classes.textField}
          value={this.state.MNAME}
          onChange={this.handleChange('MNAME')}
          margin="normal"
          variant="filled"
        />
        <TextField
          id="filled-name"
          label="LAST NAME"
          className={classes.textField}
          value={this.state.LNAME}
          onChange={this.handleChange('LNAME')}
          margin="normal"
          variant="filled"
        />
        <TextField
          id="filled-name"
          label="PHONE NUMBER"
          className={classes.textField}
          value={this.state.PHONE_NUMBER}
          onChange={this.handleChange('PHONE_NUMBER')}
          margin="normal"
          variant="filled"
        />
        <TextField
          id="filled-name"
          label="EMAIL ID"
          className={classes.textField}
          value={this.state.EMAIL_ID}
          onChange={this.handleChange('EMAIL_ID')}
          margin="normal"
          variant="filled"
        />
        <TextField
          id="filled-name"
          label="STREET"
          className={classes.textField}
          value={this.state.STREET}
          onChange={this.handleChange('STREET')}
          margin="normal"
          variant="filled"
        />
        <TextField
          id="filled-name"
          label="CITY"
          className={classes.textField}
          value={this.state.CITY}
          onChange={this.handleChange('CITY')}
          margin="normal"
          variant="filled"
        />
        <TextField
          id="filled-name"
          label="STATE_NAME"
          className={classes.textField}
          value={this.state.STATE_NAME}
          onChange={this.handleChange('STATE_NAME')}
          margin="normal"
          variant="filled"
        />
        <TextField
          id="filled-name"
          label="ZIPCODE"
          type="number"
          className={classes.textField}
          value={this.state.ZIPCODE}
          onChange={this.handleChange('ZIPCODE')}
          margin="normal"
          variant="filled"
        />
        <TextField
        id="filled-name"
        label="MEMBERSHIP TYPE"
        className={classes.textField}
        value={this.state.MEMBERSHIP_TYPE}
        onChange={this.handleChange('MEMBERSHIP_TYPE')}
        margin="normal"
        variant="filled"
        />
        <TextField
          id="filled-name"
          label="MEMBERSHIP ID"
          className={classes.textField}
          value={this.state.MEMBERSHIP_ID}
          onChange={this.handleChange('MEMBERSHIP_ID')}
          margin="normal"
          variant="filled"
        />
        <Button onClick={this.handleSubmit} variant="contained" color="primary" className={classes.button}>
          Add Customer
        </Button>
      </form>
    );
  }
}

FilledTextFields.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FilledTextFields);
