import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import Customer from './Forms/Customer'
import Location from './Forms/Location'
import Vehicle from './Forms/Vehicle'
import Insurance from './Forms/Insurance'

const styles = theme => ({
  root: {
    marginTop: 'vh',
    width: '90%',
  },
  backButton: {
    marginRight: theme.spacing.unit,
  },
  instructions: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
  },
});

function getSteps() {
  return ['Customer Details', 'Location Details', 'Vehicle Details', 'Insurance And Discount Details'];
}

function getStepContent(stepIndex) {
  switch (stepIndex) {
    case 0:
      return 'Enter customer details...';
    case 1:
      return 'Enter journey details';
    case 2:
      return 'Enter vehicle details';
    case 3:
      return 'Enter insurance and discount details'
    default:
      return 'Uknown stepIndex';
  }
}

class HorizontalLabelPositionBelowStepper extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      activeStep: 0,
      BOOKING_ID: '',
      FROM_DT_TIME: '',
      RET_DT_TIME: '',
      AMOUNT: '',
      BOOKING_STATUS: '',
      PICKUP_LOC: '',
      DROP_LOC: '',
      REG_NUM: '',
      DL_NUM: '',
      INS_CODE: '',
      ACT_RET_DT_TIME: '',
      DISCOUNT_CODE: ''
    };
  }

  handleNext = () => {
    this.setState(state => ({
      activeStep: state.activeStep + 1,
    }));
  };

  handleBack = () => {
    this.setState(state => ({
      activeStep: state.activeStep - 1,
    }));
  };

  handleReset = () => {
    this.setState({
      activeStep: 0,
    });
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
    console.log(this.state)
  };

  render() {
    const { classes } = this.props;
    const steps = getSteps();
    const { activeStep } = this.state;

    if(this.props.visible !== 2) {
      return null;
    }

    return (
      <div className={classes.root}>
        <Stepper activeStep={activeStep} alternativeLabel>
          {steps.map(label => {
            return (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            );
          })}
        </Stepper>
        <div>
            <Customer 
                visible={this.state.activeStep}
                idValue={this.state.DL_NUM}
                id={this.handleChange('DL_NUM')} />
            <Location 
                visible={this.state.activeStep}
                pickupValue={this.state.PICKUP_LOC}
                pickup={this.handleChange('PICKUP_LOC')}
                dropValue={this.state.DROP_LOC}
                drop={this.handleChange('DROP_LOC')} />
            <Vehicle 
                visible={this.state.activeStep}
                vehicle={this.handleChange('REG_NUM')}
                vehicleValue={this.state.REG_NUM} />
            <Insurance 
                visible={this.state.activeStep}
                insuranceValue={this.state.INS_CODE}
                discountValue={this.state.DISCOUNT_CODE}
                insurance={this.handleChange('INS_CODE')}
                discount={this.handleChange('DISCOUNT_CODE')} />
        </div>
        <div>
          {this.state.activeStep === steps.length ? (
            <div>
              <Typography className={classes.instructions}>All steps completed</Typography>
              <Button onClick={this.handleReset}>Reset</Button>
            </div>
          ) : (
            <div>
              <Typography className={classes.instructions}>{getStepContent(activeStep)}</Typography>
              <div>
                <Button
                  disabled={activeStep === 0}
                  onClick={this.handleBack}
                  className={classes.backButton}
                >
                  Back
                </Button>
                <Button variant="contained" color="primary" onClick={this.handleNext}>
                  {activeStep === steps.length - 1 ? 'Book' : 'Next'}
                </Button>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

HorizontalLabelPositionBelowStepper.propTypes = {
  classes: PropTypes.object,
};

export default withStyles(styles)(HorizontalLabelPositionBelowStepper);
