import React, { Component } from 'react';
import './App.css';
import Axios from 'axios';

import TabHeader from './components/Tabs'

class App extends Component {

  componentDidMount() {
    Axios.get('http://localhost:5000/show/booking').then((res) => {
      console.log(res)
    })
  }

  render() {
    return (
      <div className="App">
        <TabHeader />
      </div>
    );
  }
}

export default App;
